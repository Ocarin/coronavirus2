# Coronavirus Project


## Description

As the COVID-19 is spreading exponentially, the government needs to take a quick action who needs to be isolated and who needs to be tested for COVID-19. Due to the shortage of medical resources, the government is unable to test every single person even with symptoms, so criteria have been approved about who will be isolated and who will be tested and possibly isolated as well.

Here are the criteria. A person of age 60-69 will score one "risk point", a person of age "70-79" will score three risk points and a person over 80 will score 5 risk points. A person with diabetes will score two risk points and a person with heart disease will score 3 risk points. Thus, if a person is e.g. 65 old with diabetes, he'll score 3 risk points.

Based on the risk points, every person who scored 5 or more risk points should be isolated.

As there is a shortage of COVID-19 tests, only a portion of persons with symptoms can be tested. Only those who have a certain amount of "symptom points" can be tested. Thus, those who have "dry cough", will score 2 points. Those who have "fever", will score 3 points. Those who have "fatigue" will score 1 point. Finally, those who have "shortness of breath" will score 4 points.

Based on the symptom points, every person who scored 5 or more should be tested for COVID-19.

Furthermore, the "risk points" and the "symptom points" are summed up and every person who scored 7 or more points must be isolated.

## Objective

Now use TDD to develop a small application that is able to "decide" whether a person should be isolated, tested or both.

## Hints

This will still be quite a simple Java application, with less than 10 classes. The concrete class names could be Person, RiskAnalyzer, SymptomAnalyzer or something else, but that's up to you. Remember to use good Object-Oriented Design principles (S.O.L.I.D, abstraction, encapsulation, polymorphism, inheritance) and think about whether you should use interface(s) or abstract class(es). You don't need to unit test "trivial code" like getters and setters. It's also unlikely that you need to use Mockito for this assignment. Try to achieve at least 70% unit test code coverage. You can use e.g. ECLEmma to analyze the code coverage.

## External Links

[TDD Information](https://en.wikipedia.org/wiki/Test-driven_development)

[S.O.L.I.D Principles](https://en.wikipedia.org/wiki/SOLID)
[Spy vs. mock](https://stackoverflow.com/questions/28295625/mockito-spy-vs-mock)

[Mock vs. stub vs. fake](https://stackoverflow.com/questions/3459287/whats-the-difference-between-a-mock-stub)

## Additional Inforomation

In a nutshell (according to Martin Fowler):

**Dummy** objects are passed around but never actually used. Usually, they are just used to fill parameter lists.

**Fake** objects actually have working implementations, but usually take some shortcuts which makes them not suitable for production (an in-memory database is a good example).

**Stubs** provide canned answers to calls made during the test, usually not responding at all to anything outside what's programmed in for the test. Stubs may also record information about calls, such as an email gateway stub that remembers the messages it 'sent', or maybe only how many messages it 'sent'.

**Mocks** are what we are talking about here: objects pre-programmed with expectations which form a specification of the calls they are expected to receive.


